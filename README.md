# bot_elmt

Bot Elmt is a web application gathering open timestamped data and dispatch them through different open formats.

It is written in PHP, JS/Bootstrap and use a MariaDB or MySQL database.

Actually, Bot Elmt collect OpenWeatherMap forecasts and publish them on web site, mastodon toots, ICS calendar et RSS feeds.

## Installation

- Install source to your web site root directory
- copy config/app.conf.sample.php to config/app.conf.php
- edit config/app.conf.php
- Load timezone info if not done on your database server : mysql_tzinfo_to_sql /usr/share/zoneinfo | mysql -u root mysql
- Create a MariaDB / MySQL database and create tables with /schema.sql

## Initialise environment

- Your site should work: http://your.site.tld/
- Load cities: http://your.site.tld/?/cron/update_cities&crontoken=APP_CRON_TOKEN ( replace APP_CRON_TOKEN with value in config/app.conf.php )
- Create Mastodon application: http://your.site.tld/?/cron/get_mastodon_client_params&crontoken=APP_CRON_TOKEN and follow instruction (if not work retry loading page)
- Test a self sended toot : http://your.site.tld/?/cron/mastodon_test&crontoken=APP_CRON_TOKEN
- Add a hourly cron task calling: http://your.site.tld/?/cron/post_forecast&crontoken=APP_CRON_TOKEN ( replace APP_CRON_TOKEN with value in config/app.conf.php )

## Existing bots

You can see how it works on these sites :

- https://bot.elmt.eu/

## Included third content and software

### OpenWeatherMap.org open data

Weather/forecasts data are grabed from OpenWeatherMap.org site    
Licensed under ODbL https://opendatacommons.org/licenses/odbl/    

### Bootswatch

Bootswatch v4.1.3    
Homepage: https://bootswatch.com    
Copyright 2012-2018 Thomas Park    
Licensed under MIT    
Based on Bootstrap    

### Bootstrap

Bootstrap v4.1.3 (https://getbootstrap.com/)    
Copyright 2011-2018 The Bootstrap Authors    
Copyright 2011-2018 Twitter, Inc.    
Licensed under MIT https://github.com/twbs/bootstrap/blob/master/LICENSE      

### js-cookie library

https://github.com/js-cookie/js-cookie    
Licensed under MIT https://github.com/js-cookie/js-cookie/blob/master/LICENSE    


### ForkAwesome font

A fork of Font Awesome, originally created by Dave Gandy    
https://forkawesome.github.io/Fork-Awesome/    
Licensed under License: SIL OFL 1.1 http://scripts.sil.org/OFL    

### Weather Icons

Adilade Weather Icons Set    
https://www.adilade.fr/blog/icones-meteo-adilade-weather-icons-set/    
Licensed under CreativeCommons BY SA 3.0 https://creativecommons.org/licenses/by-sa/3.0/fr/    

### bfwk

PHP MVC Framework    
Copyright 2010-2018 Befox SARL    
Licensed under GNU/GPLv3     

### Mailer class

PHP Library AXIOM    
Author Benjamin DELESPIERRE    
Licensed under LGPL    

### Mastodon API Library

https://github.com/yks118/Mastodon-api-php    
Copyright   KwangSeon Yun    
Licensed under MIT https://raw.githubusercontent.com/yks118/Mastodon-api-php/master/LICENSE    

### SunCalc Library

SunCalc is a PHP library for calculating sun/moon position and light phases.    
https://github.com/gregseth/suncalc-php    

Based on Vladimir Agafonkin's JavaScript library.    
https://github.com/mourner/suncalc    
     
Licensed under GNU/GPLv2 https://github.com/gregseth/suncalc-php/blob/master/LICENSE    

### MoonPhase Library

Moon phase calculation class    
https://github.com/solarissmoke/php-moon-phase    

Adapted for PHP from Moontool for Windows (http://www.fourmilab.ch/moontoolw/)    
by Samir Shah (http://rayofsolaris.net)    
Licensed under MIT https://github.com/solarissmoke/php-moon-phase/blob/master/LICENSE    
