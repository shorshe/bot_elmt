<?php

define('BFWK_LANG', load_translations());

/**
 * Load translated texte according to user agent preferences
 */

function load_translations()
{
	$rLang = array();
	
	if(isset($_GET['userlang']))
	{
		$lang = $_GET['userlang'];
		$rLang[$lang] = $lang;
	}
	
	if(isset($_SESSION['userlang']))
	{
		$lang = $_SESSION['userlang'];
		$rLang[$lang] = $lang;
	}
	elseif(isset($_COOKIE['userlang']))
	{
		$lang = $_COOKIE['userlang'];
		$rLang[$lang] = $lang;
	}
	else
	{
		if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
		{
			preg_match_all('/(\W|^)([a-z]{2})([^a-z]|$)/six', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $m, PREG_PATTERN_ORDER);
			if(count($m[2])>0)
				foreach($m[2] as $lang)
					$rLang[$lang]=$lang;
		}
	}
	
	$rLang[BFWK_DEFAULT_LANG] = BFWK_DEFAULT_LANG;
		
	foreach($rLang as $lang)
	{
		$rTrans = @parse_ini_file('lang/'.$lang.'.txt');
		
		if($rTrans!==false)
		{
			$_SESSION['userlang'] = $lang;
			foreach($rTrans as $k => $v)
				define('T__'.$k, $v);
			setlocale(LC_ALL, T__app_default_locale);
			return $lang;
		}
	}
	
	
	echo "Unknown language $lang !";
	exit;

}

function get_type_translations($type, $lang=false)
{
	if($lang===false)
		$lang = $_SESSION['userlang'];
	$rTrans = @parse_ini_file('lang/'.$type.'_'.$lang.'.txt');
	return $rTrans;
}

/**
 * Get text translation
 * @param string $id : translation Id
 * @param ... :  text parameters for %1, %2 replacement
 * @return string translated text
 */

function t($id)
{
	if(defined('T__'.$id))
	{
		$txt = constant('T__'.$id);
		for($p=1; $p<func_num_args();$p++)
			$txt = str_replace('%'.$p, func_get_arg($p),$txt);
	}
	else
	{
		$txt = '_'.$id.'_';
		for($p=1; $p<func_num_args();$p++)
			$txt.= '_'.func_get_arg($p);
	}
	return $txt;
}
