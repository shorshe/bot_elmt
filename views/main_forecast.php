<?php require('header.php'); ?>


<div class="page-header" id="banner">
	<h2 class="py-2"><?=$title//The page title?></h2>
</div>

<form class="margin-clear" id="form-units" role="form" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">
	<div class="row">
		<div class="form-group col-sm-12 col-md-4">
			<div class="btn-group btn-group-toggle" data-toggle="buttons">
				<div class="input-group-prepend">
					<span class="input-group-text"><i class="fa fa-fw fa-thermometer-three-quarters "></i></span>
				</div>
				<label class="btn btn-info <?=(isset($unit_temp) && $unit_temp=='C')?'active':''?>">
					<input type="radio" name="unit_temp" id="unit_temp_C" value="C" autocomplete="off" <?=(isset($unit_temp) && $unit_temp=='C')?'checked':''?> ><small>Celsius</small>
				</label>
				<label class="btn btn-info <?=(isset($unit_temp) && $unit_temp=='F')?'active':''?>">
					<input type="radio" name="unit_temp" id="unit_temp_F" value="F" autocomplete="off" <?=(isset($unit_temp) && $unit_temp=='F')?'checked':''?> ><small>Fahrenheit</small>
				</label>
				<label class="btn btn-info <?=(isset($unit_temp) && $unit_temp=='K')?'active':''?>">
					<input type="radio" name="unit_temp" id="unit_temp_K	" value="K" autocomplete="off" <?=(isset($unit_temp) && $unit_temp=='K')?'checked':''?> ><small>Kelvin</small>
				</label>
			</div>
		</div>
		<div class="form-group col-sm-12 col-md-4">
			<div class="btn-group btn-group-toggle" data-toggle="buttons">
				<div class="input-group-prepend">
					<span class="input-group-text"><i class="fa fa-fw fa-mixcloud "></i></span>
				</div>
				<label class="btn btn-info <?=(isset($unit_speed) && $unit_speed=='m/s')?'active':''?>">
					<input type="radio" name="unit_speed" id="unit_speed_ms" value="m/s" autocomplete="off" <?=(isset($unit_speed) && $unit_speed=='m/s')?'check':''?> ><small>m/s</small>
				</label>
				<label class="btn btn-info <?=(isset($unit_speed) && $unit_speed=='km/h')?'active':''?>">
					<input type="radio" name="unit_speed" id="unit_speed_kmh" value="km/h" autocomplete="off" <?=(isset($unit_speed) && $unit_speed=='km/h')?'check':''?> ><small>km/h</small>
				</label>
				<label class="btn btn-info <?=(isset($unit_speed) && $unit_speed=='miles/hour')?'active':''?>">
					<input type="radio" name="unit_speed" id="unit_speed_mileshour" value="miles/hour" autocomplete="off" <?=(isset($unit_speed) && $unit_speed=='miles/hour')?'check':''?> ><small>miles/hour</small>
				</label>
			</div>
		</div>
		<div class="form-group col-sm-12 col-md-4">
			<div class="input-group">
				<div class="input-group-prepend">
					<span class="input-group-text"><i class="fa fa-fw fa-lg fa-clock-o"></i></span>
				</div>
				<select class="form-control <?=(!isset($timezone) || $timezone=='' || $timezone=='UTC')?'is-invalid':''?>" id="timezone" name="timezone" aria-describedby="">
					<option value="" disabled="disabled" <?=(!isset($timezone) || $timezone=='')?'selected="selected"':''?> ><?=t('Select_a_timezone')?></option>
				<?php foreach($rTimezones as $codeTZ) { ?>
					<option value="<?=$codeTZ?>" <?=(isset($timezone) && $timezone==$codeTZ)?'selected="selected"':''?> ><?=$codeTZ?></option>
				<?php } ?>
				</select>
			</div>
		</div>
	</div>
</form>

<?php
	$rDays = array();
	foreach($forecast->list as $aForecast)
		$rDays[$aForecast->dt_local_date] = $aForecast->dt_local_short_day;
?>

<div class="row">
	<div class="col-md-12 col-lg-12 col-xl-5">
		<div class="btn-group-vertical container-fluid p-0">
			<?php if(isset($unsub)) { ?>
			<a class="btn btn-danger btn-block ajax-modal" href="#?/main/modal_unsub/<?=$unsub?>/<?=$city->id?>/<?=txt::url($city->name)?>" data-toggle="modal" data-target="#myModalWindow"><i class="fa fa-fw fa-lg fa-trash"></i> <?=t('Unsubsribe_these_forecasts')?></a>
			<?php } ?>
			<a class="btn btn-primary btn-block ajax-modal" href="#?/main/modal_city/<?=$city->country?>" data-toggle="modal" data-target="#myModalWindow"><i class="fa fa-fw fa-lg fa-map-marker"></i> <?=t('Change_location')?></a>
			<a class="btn btn-light btn-block disabled"><?=t('Suscribe_to_these_forecasts_through')?></a>
			<a style="display:none;" class="btn btn-light btn-block ajax-modal" href="#?/main/modal_mail/<?=$city->id?>/<?=$unit_temp?>/<?=$unit_speed?>/<?=$userlang?>/<?=$timezone?>" data-toggle="modal" data-target="#myModalWindow"><i class="fa fa-fw fa-lg fa-envelope"></i> <?=t('Daily_mail')?></a>
			<a class="btn btn-light btn-block ajax-modal" href="#?/main/modal_mastodon/<?=$city->id?>/<?=$unit_temp?>/<?=$unit_speed?>/<?=$userlang?>/<?=$timezone?>" data-toggle="modal" data-target="#myModalWindow"><i class="fa fa-fw fa-lg fa-mastodon"></i> <?=t('Daily_Mastodon_toot')?></a>
			<a class="btn btn-light btn-block ajax-modal" href="#?/main/modal_ics/<?=$city->id?>/<?=$unit_temp?>/<?=$unit_speed?>/<?=$userlang?>/<?=$timezone?>" data-toggle="modal" data-target="#myModalWindow"><i class="fa fa-fw fa-lg fa-calendar-plus-o"></i> <?=t('ICS_Calendar')?></a>
			<a class="btn btn-light btn-block ajax-modal" href="#?/main/modal_rss/<?=$city->id?>/<?=$unit_temp?>/<?=$unit_speed?>/<?=$userlang?>/<?=$timezone?>" data-toggle="modal" data-target="#myModalWindow"><i class="fa fa-fw fa-lg fa-rss"></i> <?=t('RSS_Feed')?></a>
			<a class="btn btn-light btn-block ajax-modal" href="#?/main/modal_txt/<?=$city->id?>/<?=$unit_temp?>/<?=$unit_speed?>/<?=$userlang?>/<?=$timezone?>" data-toggle="modal" data-target="#myModalWindow"><i class="fa fa-fw fa-lg fa-file-text-o"></i> <?=t('Txt_export')?></a>
		</div>
	</div>
	<div class="col-md-12 col-lg-1 col-xl-1">
		<h4><i class="fa fa-fw fa-calendar"></i></h4>
		<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
			<?php $firstDay=true;
			foreach($rDays as $day) { ?>
				<a class="nav-link <?=$firstDay?'active':''?>" id="v-pills-<?=txt::url($day)?>-tab" data-toggle="pill" href="#v-pills-<?=txt::url($day)?>" role="tab" aria-controls="v-pills-<?=txt::url($day)?>" aria-selected="<?=$firstDay?'true':'false'?>"><?=$day?></a>
			<?php $firstDay=false; } ?>
		</div>
	</div>
	<div class="col-md-12 col-lg-11 col-xl-6">
		<div class="tab-content" id="v-pills-tabContent">
			<?php $firstDay=true;
			foreach($rDays as $dt_local_date => $day) { ?>
				<div class="tab-pane fade show <?=$firstDay?'active':''?>" id="v-pills-<?=txt::url($day)?>" role="tabpanel" aria-labelledby="v-pills-<?=txt::url($day)?>-tab">
					<div>
						<h4><?=$day?> <?=substr($dt_local_date,-2)?></h4>
						<div class="row">
							<div class="col-6 block-suninfo" alt="<?=t('Sun_rise_noon_and_set')?>" title="<?=t('Sun_rise_noon_and_set')?>">
								<i class="fa fa-fw fa-sun-o"></i>
								<sub><i class="fa fa-fw fa-arrow-circle-up"></i><?=$forecast->suninfo[$dt_local_date]->sunrise->format('H:i')?></sub>
								<sup><i class="fa fa-fw fa-chevron-circle-up"></i><?=$forecast->suninfo[$dt_local_date]->solarNoon->format('H:i')?></sup>
								<sub><i class="fa fa-fw fa-arrow-circle-down"></i><?=$forecast->suninfo[$dt_local_date]->sunset->format('H:i')?></sub>
							</div>
							<div class="col-6 block-mooninfo" alt="<?=t('Moon_rise_and_set')?>" title="<?=t('Moon_rise_and_set')?>">
								<i class="fa fa-fw fa-moon-o"></i>
								<?php if(  $forecast->mooninfo[$dt_local_date]->moonrise!==false
										&& $forecast->mooninfo[$dt_local_date]->moonset!==false
										&& $forecast->mooninfo[$dt_local_date]->moonrise->format('H:i') <  $forecast->mooninfo[$dt_local_date]->moonset->format('H:i')) { ?>
									<small>
										<i class="fa fa-fw fa-arrow-circle-up"></i>
										<?=$forecast->mooninfo[$dt_local_date]->moonrise->format('H:i')?>
										/
										<i class="fa fa-fw fa-arrow-circle-down"></i>
										<?=$forecast->mooninfo[$dt_local_date]->moonset->format('H:i')?>
									</small>
								<?php } elseif(  $forecast->mooninfo[$dt_local_date]->moonrise!==false
										&& $forecast->mooninfo[$dt_local_date]->moonset!==false
										&& $forecast->mooninfo[$dt_local_date]->moonrise->format('H:i') >  $forecast->mooninfo[$dt_local_date]->moonset->format('H:i')) { ?>
									<small>
										<i class="fa fa-fw fa-arrow-circle-down"></i>
										<?=$forecast->mooninfo[$dt_local_date]->moonset->format('H:i')?>
										/
										<i class="fa fa-fw fa-arrow-circle-up"></i>
										<?=$forecast->mooninfo[$dt_local_date]->moonrise->format('H:i')?>
									</small>
								<?php } elseif( $forecast->mooninfo[$dt_local_date]->moonrise!==false) { ?>
									<small>
										<i class="fa fa-fw fa-arrow-circle-up"></i>
										<?=$forecast->mooninfo[$dt_local_date]->moonrise->format('H:i')?>
									</small>
								<?php } elseif( $forecast->mooninfo[$dt_local_date]->moonset!==false) { ?>
									<small>
										<i class="fa fa-fw fa-arrow-circle-down"></i>
										<?=$forecast->mooninfo[$dt_local_date]->moonset->format('H:i')?>
									</small>
								<?php } elseif( $forecast->mooninfo[$dt_local_date]->alwaysUp ) { ?>
									<sup><i class="fa fa-fw fa-chevron-circle-up"></i></sup>
								<?php } elseif( $forecast->mooninfo[$dt_local_date]->alwaysDown ) { ?>
									<sub><i class="fa fa-fw fa-chevron-circle-down"></i></sub>
								<?php } ?>
							</div>
							<div class="col-6 block-lunarnode">
								<i class="fa fa-fw fa-heartbeat"></i>
							</div>
							<div class="col-6 block-nextmoon">
								<i class="fa fa-fw fa-step-forward"></i>
								<img src="<?=$forecast->mooninfo[$dt_local_date]->next_phase_img?>" width="20" height="20" alt="<?=$forecast->mooninfo[$dt_local_date]->next_phase_name?>" title="<?=$forecast->mooninfo[$dt_local_date]->next_phase_name?>" />
								<small alt="<?=t('Next_moon_phase').' : '.$forecast->mooninfo[$dt_local_date]->next_phase_dt->format('Y-m-d H:i')?>" title="<?=t('Next_moon_phase').' : '.$forecast->mooninfo[$dt_local_date]->next_phase_dt->format('Y-m-d H:i')?>">
									<?=$forecast->mooninfo[$dt_local_date]->next_phase_long_day?>
								</small>
							</div>
						</div>
					</div>
					<table id="forecast_table" class="container-fluid">
						<tr>
							<th alt="<?=t('Time')?>" title="<?=t('Time')?>"><i class="fa fa-fw fa-clock-o"></i></th>
							<th alt="<?=t('Weather_condition')?>" title="<?=t('Weather_condition')?>"><i class="fa fa-fw fa-sun-o"></i>/<i class="fa fa-fw fa-cloud"></i></th>
							<th alt="<?=t('Rain_for_3_previous_hours')?>" title="<?=t('Rain_for_3_previous_hours')?>"><i class="fa fa-fw fa-cloud-download"></i></th>
							<th alt="<?=t('Temperature')?>" title="<?=t('Temperature')?>"><i class="fa fa-fw fa-thermometer-three-quarters"></i></th>
							<th class="d-none d-md-table-cell" alt="<?=t('Humidity')?>" title="<?=t('Humidity')?>"><i class="fa fa-fw fa-shower"></i></th>
							<th alt="<?=t('Wind')?>" title="<?=t('Wind')?>"><i class="fa fa-fw fa-mixcloud"></i></th>
							<th alt="<?=t('Moon_phase')?>" title="<?=t('Moon_phase_and_illumination')?>"><i class="fa fa-fw fa-moon-o"></i></th>
						</tr>
						<?php foreach($forecast->list as $aForecast) { 
							if($aForecast->dt_local_date!=$dt_local_date)
								continue;
						?>
						<tr class="line_hour_<?=intval($aForecast->dt_local_short_time)?>">
							<th>
								<?=$aForecast->dt_local_short_time?>
							</th>
							<td>
								<?php if(count($aForecast->weather)>0)
									foreach($aForecast->weather as &$weather) { ?>
										<img src="<?=$weather->img?>" width="32" height="32" alt="<?=$weather->local_description?>" title="<?=$weather->local_description?>" /><span class="d-none d-md-inline-block"> <?=$weather->local_description?></span><br />
								<?php } ?>
							</td>
							<td>
								<?=$aForecast->rain->volume?>
							</td>
							<td>
								<?=$aForecast->main->temp?>
							</td>
							<td class="d-none d-md-table-cell">
								<?=$aForecast->main->humidity?>%
							</td>
							<td>
								<img src="<?=$aForecast->wind->img?>" width="32" height="32" alt="<?=$aForecast->wind->dirtxt?>" title="<?=$aForecast->wind->dirtxt?>" /> <?=$aForecast->wind->speed?>
							</td>
							<td>
								<img src="<?=$aForecast->moondetail->phase_img?>" width="24" height="24" alt="<?=$aForecast->moondetail->phase_name?>" title="<?=$aForecast->moondetail->phase_name?>" /><span class="d-none d-md-inline-block"> <?=round($aForecast->moondetail->illumination*100).'%'?></span>
							</td>
						</tr>
						<?php } ?>
					</table>
				</div>
			<?php $firstDay=false; } ?>
		</div>
	</div>
</div>

<script>
	function onload() {
		$('#form-units input, #form-units select').change(function() {
			$('#form-units').submit();
		});
	}
</script>

<?php require('footer.php'); 
