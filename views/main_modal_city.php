	<div class="modal-dialog  ctrl_<?=$_ctrl?> view_<?=$_viewpage?>" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><i class="fa fa-fw fa-lg fa-map-marker"></i> <?=t('Choose_your_location')?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		<?php
			if(isset($err)) { ?>
			<div class="alert alert-dismissible alert-danger">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?=$err?>
			</div>
		<?php }
			if(isset($wrn)) { ?>
			<div class="alert alert-dismissible alert-warning">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?=$wrn?>
			</div>
		<?php }
			if(isset($msg)) { ?>
			<div class="alert alert-dismissible alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?=$msg?>
			</div>
		<?php }	?>
			
			
		<form class="margin-clear" id="form-form" role="form" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">

			<div class="modal-body">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fa fa-fw fa-lg fa-globe"></i></span>
						</div>
						<select class="form-control <?=(!isset($country) || $country=='')?'is-invalid':''?>" id="country" name="country" aria-describedby="">
							<option value="" disabled="disabled" <?=(!isset($country) || $country=='')?'selected="selected"':''?> ><?=t('Select_a_country')?></option>
						<?php foreach($rCountries as $codeCountry => $libCountry) { ?>
							<option value="<?=substr($codeCountry,1)?>" <?=(isset($country) && $country==substr($codeCountry,1))?'selected="selected"':''?> ><?=$libCountry?></option>
						<?php } ?>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<div class="input-group">
						<input class="form-control" id="city_search" name="city_search" aria-describedby="" placeholder="<?=t('Type_the_name_of_the_city')?>" type="text" size="30" value="<?=isset($city_search)?$city_search:''?>">
						<div class="input-group-append">
							<button class="btn btn-outline-primary" type="submit"><i class="fa fa-fw fa-lg fa-search"></i></button>
						</div>
					</div>
				</div>
				<?php if(isset($rCities) && count($rCities)>0) { ?>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fa fa-fw fa-lg fa-map-marker"></i></span>
						</div>
						<select class="form-control is-invalid" id="city" name="city" aria-describedby="">
							<option value="" disabled="disabled" <?=(!isset($city) || $city==-1)?'selected="selected"':''?> ><?=t('Select_a_city')?></option>
						<?php foreach($rCities as $acity) { ?>
							<option value="<?=$acity->id?>" <?=(isset($city) && $city==$acity->id)?'selected="selected"':''?> ><?=$acity->name?> [<?=$acity->country?>] (<?=$acity->lat*1.0?>/<?=$acity->lon*1.0?>)</option>
						<?php } ?>
						</select>
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="modal-footer">
				<?php if(isset($rCities) && count($rCities)>0) { ?>
				<button type="submit" id="choose" name="choose" class="btn btn-primary"><?=t('Choose')?></button>
				<?php } ?>
				<button type="button" class="btn btn-secondary" data-dismiss="modal"><?=t('Close')?></button>
			</div>
		</form>
		</div>
	</div>

<script>
	// display result in modal
	modalize_form('#form-form');
</script>
