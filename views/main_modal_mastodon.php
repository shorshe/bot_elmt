	<div class="modal-dialog  ctrl_<?=$_ctrl?> view_<?=$_viewpage?>" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><i class="fa fa-fw fa-lg fa-mastodon"></i> <?=t('Daily_Mastodon_toot')?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		<?php
			if(isset($err)) { ?>
			<div class="alert alert-dismissible alert-danger">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?=$err?>
			</div>
		<?php }
			if(isset($wrn)) { ?>
			<div class="alert alert-dismissible alert-warning">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?=$wrn?>
			</div>
		<?php }
			if(isset($msg)) { ?>
			<div class="alert alert-dismissible alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button> 
				<?=$msg?>
			</div>
		<?php }	?>
			
			<div class="alert alert-dismissible alert-warning">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<p>
				<?=t('You_must_follow_to_get_direct_messages',APP_MASTODON_USER_PROFIL)?> <a href="<?=trim(APP_MASTODON_INSTANCE_URL,'/').'/@'.APP_MASTODON_USER_PROFIL?>" target="_blank"><i class="fa fa-fw fa-lg fa-external-link"></i></a>
				</p>
			</div>
			
		<form class="margin-clear" id="form-form" role="form" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">

			<div class="modal-body">
				
				<?php if(!isset($msg)) { ?>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text"><i class="fa fa-fw fa-lg fa-mastodon-square"></i></span>
							</div>
							<input class="form-control is-invalid" id="address" name="address" aria-describedby="" placeholder="<?=t('masto_account_placeholder')?>" type="text" size="60" value="<?=isset($address)?$address:''?>">
						</div>
					</div>
					
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text"><i class="fa fa-fw fa-lg fa-clock-o"></i></span>
							</div>
							<select class="form-control <?=(!isset($local_time_range) || $local_time_range==-1)?'is-invalid':''?>" id="local_time_range" name="local_time_range" aria-describedby="">
								<option value="-1" <?=(!isset($local_time_range) || $local_time_range==-1)?'selected="selected"':''?> ><?=t('Select_local_time_you_want_your_toot')?></option>
							<?php for($h=0; $h<24; $h++) { ?>
								<option value="<?=$h?>" <?=(isset($local_time_range) && $local_time_range==$h)?'selected="selected"':''?> ><?=sprintf('%02d',$h)?>:00</option>
							<?php } ?>
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<div class="btn-group btn-group-toggle" data-toggle="buttons">
							<div class="input-group-prepend">
								<span class="input-group-text"><i class="fa fa-fw fa-sun-o "></i><i class="fa fa-fw fa-cloud"></i></span>
							</div>
							<label class="btn btn-info active">
								<input type="radio" name="pref_emojis" id="pref_emoji_1" value="1" autocomplete="off" checked><?=t('Use_emojis')?>
							</label>
							<label class="btn btn-info">
								<input type="radio" name="pref_emojis" id="pref_emoji_0" value="0" autocomplete="off"><?=t('Only_text')?>
							</label>
							<div class="input-group-append">
								<span class="input-group-text">abc...</span>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="btn-group btn-group-toggle" data-toggle="buttons">
							<div class="input-group-prepend">
								<span class="input-group-text"><i class="fa fa-fw fa-thermometer-three-quarters"></i>&nbsp;&nbsp;<?=t('Temperature')?></span>
							</div>
							<label class="btn btn-info active">
								<input type="radio" name="pref_temperature" value="1" autocomplete="off" checked><?=t('bYes')?>
							</label>
							<label class="btn btn-info">
								<input type="radio" name="pref_temperature" value="0" autocomplete="off"><?=t('bNo')?>
							</label>
						</div>
					</div>
					
					<div class="form-group">
						<div class="btn-group btn-group-toggle" data-toggle="buttons">
							<div class="input-group-prepend">
								<span class="input-group-text"><i class="fa fa-fw fa-cloud-download"></i>&nbsp;&nbsp;<?=t('Rain_for_3_previous_hours')?></span>
							</div>
							<label class="btn btn-info active">
								<input type="radio" name="pref_rain3h" value="1" autocomplete="off" checked><?=t('bYes')?>
							</label>
							<label class="btn btn-info">
								<input type="radio" name="pref_rain3h" value="0" autocomplete="off"><?=t('bNo')?>
							</label>
						</div>
					</div>
					
					<div class="form-group">
						<div class="btn-group btn-group-toggle" data-toggle="buttons">
							<div class="input-group-prepend">
								<span class="input-group-text"><i class="fa fa-fw fa-shower"></i>&nbsp;&nbsp;<?=t('Humidity')?></span>
							</div>
							<label class="btn btn-info">
								<input type="radio" name="pref_humidity" value="1" autocomplete="off"><?=t('bYes')?>
							</label>
							<label class="btn btn-info active">
								<input type="radio" name="pref_humidity" value="0" autocomplete="off" checked><?=t('bNo')?>
							</label>
						</div>
					</div>
					
					<div class="form-group">
						<div class="btn-group btn-group-toggle" data-toggle="buttons">
							<div class="input-group-prepend">
								<span class="input-group-text"><i class="fa fa-fw fa-mixcloud"></i>&nbsp;&nbsp;<?=t('Wind')?></span>
							</div>
							<label class="btn btn-info active">
								<input type="radio" name="pref_wind" value="1" autocomplete="off" checked><?=t('bYes')?>
							</label>
							<label class="btn btn-info">
								<input type="radio" name="pref_wind" value="0" autocomplete="off"><?=t('bNo')?>
							</label>
						</div>
					</div>
				<?php } ?>
				
			</div>
			<div class="modal-footer">
				<?php if(!isset($msg)) { ?>
				<button type="submit" id="choose" name="choose" class="btn btn-primary"><?=t('OK')?></button>
				<?php }	?>
				<button type="button" class="btn btn-secondary" data-dismiss="modal"><?=t('Close')?></button>
			</div>
		</form>
		</div>
	</div>

<script>
	// display result in modal
	modalize_form('#form-form');
</script>
