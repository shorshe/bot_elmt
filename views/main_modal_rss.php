	<div class="modal-dialog  ctrl_<?=$_ctrl?> view_<?=$_viewpage?>" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><i class="fa fa-fw fa-lg fa-rss"></i> <?=t('RSS_Feed')?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		<?php
			if(isset($err)) { ?>
			<div class="alert alert-dismissible alert-danger">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?=$err?>
			</div>
		<?php }
			if(isset($wrn)) { ?>
			<div class="alert alert-dismissible alert-warning">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?=$wrn?>
			</div>
		<?php }
			if(isset($msg)) { ?>
			<div class="alert alert-dismissible alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button> 
				<?=$msg?>
			</div>
		<?php }	?>
			
			
		<form class="margin-clear" id="form-form" role="form" method="post" action="<?=$_SERVER["REQUEST_URI"]?>">

			<div class="modal-body">
				
				<?php if(!isset($urlrss)) { ?>
					<div class="form-group">
						<div class="btn-group btn-group-toggle" data-toggle="buttons">
							<div class="input-group-prepend">
								<span class="input-group-text"><i class="fa fa-fw fa-sun-o "></i><i class="fa fa-fw fa-cloud"></i></span>
							</div>
							<label class="btn btn-info active">
								<input type="radio" name="pref_emojis" id="pref_emoji_1" value="1" autocomplete="off" checked><?=t('Use_emojis')?>
							</label>
							<label class="btn btn-info">
								<input type="radio" name="pref_emojis" id="pref_emoji_0" value="0" autocomplete="off"><?=t('Only_text')?>
							</label>
							<div class="input-group-append">
								<span class="input-group-text">abc...</span>
							</div>
						</div>
					</div>
				<?php }	else { ?>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text"><i class="fa fa-fw fa-rss"></i><i class="fa fa-fw fa-link"></i></span>
							</div>
							<input class="form-control" aria-describedby="" type="text" value="<?=$urlrss?>">
							<div class="input-group-append">
								<a class="btn btn-primary" href="<?=$urlrss?>" target="_blank"><i class="fa fa-fw fa-lg fa-external-link"></i></a>
							</div>
						</div>
					</div>
				<?php } ?>
				
			</div>
			<div class="modal-footer">
				<?php if(!isset($urlrss)) { ?>
				<button type="submit" id="choose" name="choose" class="btn btn-primary"><?=t('OK')?></button>
				<?php }	?>
				<button type="button" class="btn btn-secondary" data-dismiss="modal"><?=t('Close')?></button>
			</div>
		</form>
		</div>
	</div>

<script>
	// display result in modal
	modalize_form('#form-form');
</script>
